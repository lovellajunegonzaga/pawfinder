package com.example.asusadmin.fireapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.client.Firebase;

public class MainActivity extends AppCompatActivity {

    private EditText mLocationField;
    private Button mAddBtn;
    private EditText mNameValue;
    private TextView mDateLabel;

    private Firebase mRootRef;
    //private Firebase mRootRef1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRootRef = new Firebase("https://fireapp-c2ad7.firebaseio.com/");
        //mRootRef1 = new Firebase("https://fireapp-c2ad7.firebaseio.com/Name");

        mLocationField = (EditText) findViewById(R.id.locationField);
        mAddBtn = (Button) findViewById(R.id.addBtn);
        mNameValue = (EditText) findViewById(R.id.nameValue);
        mDateLabel = (TextView) findViewById(R.id.dateLabel);

        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String location = mLocationField.getText().toString();
                String name = mNameValue.getText().toString();
                String date = mDateLabel.getText().toString();

                //Firebase childRef1 = mRootRef1.child(value);
                Firebase childRef = mRootRef.child(name);

                childRef.push().setValue(date);
                childRef.push().setValue(location);
                //childRef.setValue(value);
            }
        });

    }
}
