package com.example.asusadmin.fireapp;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by asus.admin on 9/17/2016.
 */
public class FireApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Firebase.setAndroidContext(this);
    }
}
